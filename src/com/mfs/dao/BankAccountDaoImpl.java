package com.mfs.dao;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import com.mfs.pojo.BankAccount;
import com.mfs.utility.HibernateUtil;

/**
 * bankaccount implementaton / this class is used to connect with database
 * 
 * @author shrikant
 *
 */
public class BankAccountDaoImpl implements BankAccountDao {

	private Session session = null;

	public BankAccountDaoImpl() {
		session = HibernateUtil.getSessionFactory().openSession();
	}

	@Transactional
	public void encyptPwd(int id,String username,String pwd) {
		session.beginTransaction();
		BankAccount accounts = new BankAccount();
		String str = "INSERT INTO user VALUES" + "(" + id + "," + "'" +username +"'" + "," + "AES_ENCRYPT" + "(" +"'"+ pwd+"'"
				+ "," + "SHA2 " + "( " + "'password'" + "," + 256 + ")" + ")" + ")";
		System.out.println(str);
		Query qry = session.createNativeQuery(str);
		int res = qry.executeUpdate();
		System.out.println(res);
		session.getTransaction().commit();
		session.close();

	}

	@Transactional
	public void decryptPwd(int id) {

		session.beginTransaction();
		BankAccount accounts = new BankAccount();
		String str = "select cast" + "(" + "aes_decrypt" + "(" + "password" + "," + "sha2" + "(" + "'password'" + ","
				+ 256 + ")" + ")" + "as char" + "(" + 60 + ")" + ")" + "FROM user where userid=" + id;
		Query query = session.createNativeQuery(str);
		Object data = query.getSingleResult();
		System.out.println("Decrypted password-----> " +data);
		session.close();

	}
}