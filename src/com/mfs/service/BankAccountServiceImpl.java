package com.mfs.service;

import com.mfs.dao.BankAccountDaoImpl;

/**
 * class for service implementation
 * @author Shrikant
 *
 */
public class BankAccountServiceImpl implements BankAccountService {

	@Override
	public void encyptPwd(int id,String username,String pwd) {
		new BankAccountDaoImpl().encyptPwd(id,username,pwd);

	}

	@Override
	public void decryptPwd(int id) {
		new BankAccountDaoImpl().decryptPwd(id);
	}

}
